-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 21 fév. 2022 à 14:57
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mabd`
--

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

CREATE TABLE `enseignants` (
  `Id` int(11) NOT NULL,
  `Nom` varchar(30) DEFAULT NULL,
  `Prenom` varchar(20) DEFAULT NULL,
  `Sexe` varchar(20) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `Id_ue` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `enseignants` (`Id`, `Nom`, `Prenom`, `Sexe`, `Username`, `Password`, `Id_ue`) VALUES
(1, 'afate', 'ayelim', 'masculin', '@afate', '1234', 'Math');

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

CREATE TABLE `etudiants` (
  `Id` int(11) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Prenom` varchar(20) NOT NULL,
  `Sexe` varchar(20) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`Id`, `Nom`, `Prenom`, `Sexe`, `Username`, `Password`) VALUES
(1, '', 'kofi', 'mascuin', '@yao', '1234');

-- --------------------------------------------------------

--
-- Structure de la table `inbox`
--

CREATE TABLE `inbox` (
  `Id_enseignant` int(11) DEFAULT NULL,
  `Contenu` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `inbox`
--

INSERT INTO `inbox` (`Id_enseignant`, `Contenu`) VALUES
(1, 'Bonjour Monsieur Je viens vers vous faire une reclamation ,Vous avez attribuer une note dont je suis pas sur [Nom :, Prenom:kofi,Note:15.0]');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE `notes` (
  `Id_etudiant` int(11) DEFAULT NULL,
  `Ue_id` varchar(5) DEFAULT NULL,
  `Note` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `notes`
--

INSERT INTO `notes` (`Id_etudiant`, `Ue_id`, `Note`) VALUES
(1, 'Math', 15);

-- --------------------------------------------------------

--
-- Structure de la table `reclamations`
--

CREATE TABLE `reclamations` (
  `Id_etudiant` int(11) DEFAULT NULL,
  `Nom_UE` varchar(5) DEFAULT NULL,
  `NoteConteste` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reclamations`
--

INSERT INTO `reclamations` (`Id_etudiant`, `Nom_UE`, `NoteConteste`) VALUES
(1, 'Math', 15);

-- --------------------------------------------------------

--
-- Structure de la table `ues`
--

CREATE TABLE `ues` (
  `Nom_UE` varchar(5) NOT NULL,
  `Credits` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ues`
--

INSERT INTO `ues` (`Nom_UE`, `Credits`) VALUES
('Math', 4);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `enseignants`
--
ALTER TABLE `enseignants`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_ue` (`Id_ue`);

--
-- Index pour la table `etudiants`
--
ALTER TABLE `etudiants`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `inbox`
--
ALTER TABLE `inbox`
  ADD KEY `Id_enseignant` (`Id_enseignant`);

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
  ADD KEY `Id_etudiant` (`Id_etudiant`),
  ADD KEY `Ue_id` (`Ue_id`);

--
-- Index pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD KEY `Id_etudiant` (`Id_etudiant`);

--
-- Index pour la table `ues`
--
ALTER TABLE `ues`
  ADD PRIMARY KEY (`Nom_UE`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `enseignants`
--
ALTER TABLE `enseignants`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `etudiants`
--
ALTER TABLE `etudiants`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `enseignants`
--
ALTER TABLE `enseignants`
  ADD CONSTRAINT `enseignants_ibfk_1` FOREIGN KEY (`Id_ue`) REFERENCES `ues` (`Nom_UE`);

--
-- Contraintes pour la table `inbox`
--
ALTER TABLE `inbox`
  ADD CONSTRAINT `inbox_ibfk_1` FOREIGN KEY (`Id_enseignant`) REFERENCES `enseignants` (`Id`);

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`Id_etudiant`) REFERENCES `etudiants` (`Id`),
  ADD CONSTRAINT `notes_ibfk_2` FOREIGN KEY (`Ue_id`) REFERENCES `ues` (`Nom_UE`);

--
-- Contraintes pour la table `reclamations`
--
ALTER TABLE `reclamations`
  ADD CONSTRAINT `reclamations_ibfk_1` FOREIGN KEY (`Id_etudiant`) REFERENCES `etudiants` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
